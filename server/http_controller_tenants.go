package server

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
)

func GetTenantSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.TenantSchema

		return nil
	})

	uc.SetTitle("Get Tenant Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getTenantByIDInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetTenantByID(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getTenantByIDInput, output *models.Tenant) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createTenantInput struct {
	ID         string `json:"id" description:"..." required:"true"`
	Active     bool   `json:"active" description:"..." required:"true"`
	Endpoint   string `json:"endpoint" description:"..." required:"true"`
	Production bool   `json:"production" description:"..." required:"true"`
	Username   string `json:"username" description:"..." required:"true"`
	Password   string `json:"password" description:"..." required:"true"`
	TaxNumber  string `json:"tax_number" description:"..." required:"true"`
	Code       string `json:"code" description:"..." required:"true"`
	AppName    string `json:"app_name" description:"..." required:"true"`
	AppService string `json:"app_service" description:"..." required:"true"`
	AppKey     string `json:"app_key" description:"..." required:"true"`
}

func CreateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		_, err := tenantsCache.Get(ctx, input.ID)

		if err == nil || err.Error() != "value not found in store" {
			return status.AlreadyExists
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Endpoint = input.Endpoint
		output.Production = input.Production
		output.Username = input.Username
		output.Password = input.Password
		output.TaxNumber = input.TaxNumber
		output.Code = input.Code
		output.AppName = input.AppName
		output.AppService = input.AppService
		output.AppKey = input.AppKey

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type updateTenantInput struct {
	TenantId   string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID         string `json:"id" description:"..." required:"true"`
	Active     bool   `json:"active" description:"..." required:"true"`
	Endpoint   string `json:"endpoint" description:"..." required:"true"`
	Production bool   `json:"production" description:"..." required:"true"`
	Username   string `json:"username" description:"..." required:"true"`
	Password   string `json:"password" description:"..." required:"true"`
	TaxNumber  string `json:"tax_number" description:"..." required:"true"`
	Code       string `json:"code" description:"..." required:"true"`
	AppName    string `json:"app_name" description:"..." required:"true"`
	AppService string `json:"app_service" description:"..." required:"true"`
	AppKey     string `json:"app_key" description:"..." required:"true"`
}

func UpdateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		if input.ID != tenant.ID {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Endpoint = input.Endpoint
		output.Production = input.Production
		output.Username = input.Username
		output.Password = input.Password
		output.TaxNumber = input.TaxNumber
		output.Code = input.Code
		output.AppName = input.AppName
		output.AppService = input.AppService
		output.AppKey = input.AppKey

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchTenantInput struct {
	TenantId   string  `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active     *bool   `json:"active" description:"..."`
	Endpoint   *string `json:"endpoint" description:"..."`
	Production *bool   `json:"production" description:"..."`
	Username   *string `json:"username" description:"..."`
	Password   *string `json:"password" description:"..."`
	TaxNumber  *string `json:"tax_number" description:"..."`
	Code       *string `json:"code" description:"..."`
	AppName    *string `json:"app_name" description:"..."`
	AppService *string `json:"app_service" description:"..."`
	AppKey     *string `json:"app_key" description:"..."`
}

func PatchTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		output.ID = tenant.ID

		if input.Active == nil {
			output.Active = tenant.Active
		} else {
			output.Active = *input.Active
		}
		if input.Endpoint == nil {
			output.Endpoint = tenant.Endpoint
		} else {
			output.Endpoint = *input.Endpoint
		}
		if input.Production == nil {
			output.Production = tenant.Production
		} else {
			output.Production = *input.Production
		}
		if input.Username == nil {
			output.Username = tenant.Username
		} else {
			output.Username = *input.Username
		}
		if input.Password == nil {
			output.Password = tenant.Password
		} else {
			output.Password = *input.Password
		}
		if input.TaxNumber == nil {
			output.TaxNumber = tenant.TaxNumber
		} else {
			output.TaxNumber = *input.TaxNumber
		}
		if input.Code == nil {
			output.Code = tenant.Code
		} else {
			output.Code = *input.Code
		}
		if input.AppName == nil {
			output.AppName = tenant.AppName
		} else {
			output.AppName = *input.AppName
		}
		if input.AppService == nil {
			output.AppService = tenant.AppService
		} else {
			output.AppService = *input.AppService
		}
		if input.AppKey == nil {
			output.AppKey = tenant.AppKey
		} else {
			output.AppKey = *input.AppKey
		}

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type disableTenantInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableTenantInput, _ *struct{}) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		tenant.Active = false

		err = StoreTenant(sctx, tenant)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
