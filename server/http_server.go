package server

import (
	"net/http"
	"time"
)

func StartHttpServer(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()

	service := initService(sctx)

	registerRoutes(sctx, service)

	httpServer := &http.Server{
		Addr:    serverConfig.HttpBind + ":" + serverConfig.HttpPort,
		Handler: service,
	}

	go func() {
		err := httpServer.ListenAndServe()

		if err != nil && err != http.ErrServerClosed {
			sctx.LogHttpFatal().Stack().Err(err).Msg("http server boot failed")
		}
	}()

	<-sctx.Done()
	sctx.LogHttpDebug().Msg("http server received exit signal, shutting down")
	stopWebServer(sctx, httpServer)
	sctx.LogHttpDebug().Msg("http server shutted down")
}

func stopWebServer(sctx *ServerContext, server *http.Server) {
	ctx, cancel := sctx.WithTimeout(5 * time.Second)
	defer cancel()

	err := server.Shutdown(ctx)
	if err != nil {
		sctx.LogHttpFatal().Stack().Err(err).Msg("http server shutdown failed")
	}
}
