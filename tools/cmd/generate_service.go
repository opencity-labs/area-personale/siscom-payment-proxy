package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
)

var generateServiceCmd = &cobra.Command{
	Use:   "service",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated service json:")
		fmt.Println()

		tenant_id, err := cmd.Flags().GetString("tenant_id")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		entry_code, err := cmd.Flags().GetString("entry_code")
		if err != nil {
			panic(err)
		}
		key, err := cmd.Flags().GetString("key")
		if err != nil {
			panic(err)
		}
		description, err := cmd.Flags().GetString("description")
		if err != nil {
			panic(err)
		}
		note, err := cmd.Flags().GetString("note")
		if err != nil {
			panic(err)
		}
		acc_entry, err := cmd.Flags().GetInt32("acc_entry")
		if err != nil {
			panic(err)
		}
		acc_cap, err := cmd.Flags().GetInt32("acc_cap")
		if err != nil {
			panic(err)
		}
		acc_art, err := cmd.Flags().GetInt32("acc_art")
		if err != nil {
			panic(err)
		}
		split, err := cmd.Flags().GetBool("split")
		if err != nil {
			panic(err)
		}

		serviceSplits := []models.ServiceSplit{}
		if split {
			serviceSplits = []models.ServiceSplit{
				{
					Code:        "c_1",
					Amount:      16.00,
					Key:         "c_1",
					Description: "Causale c_1",
					AccEntry:    1230,
					AccCap:      245,
					AccArt:      1,
				},
				{
					Code:        "a_1",
					Amount:      0.50,
					Key:         "a_1",
					Description: "Causale a_1",
					AccEntry:    1230,
					AccCap:      245,
					AccArt:      2,
				},
			}
		}

		service := models.Service{
			ID:          uuid.NewV4().String(),
			TenantID:    tenant_id,
			Active:      true,
			Code:        code,
			EntryCode:   entry_code,
			Key:         key,
			Description: description,
			Note:        note,
			AccEntry:    acc_entry,
			AccCap:      acc_cap,
			AccArt:      acc_art,
			Splitted:    split,
			Split:       serviceSplits,
		}

		bytes, err := json.MarshalIndent(service, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateServiceCmd)

	generateServiceCmd.Flags().StringP("tenant_id", "t", "", "ID del tenant")
	generateServiceCmd.MarkFlagRequired("tenant_id")
	generateServiceCmd.Flags().StringP("code", "c", "1", "Codice servizio")
	generateServiceCmd.Flags().StringP("entry_code", "e", "", "Codice entrata")
	generateServiceCmd.Flags().StringP("key", "k", "", "Codice tipo procedura")
	generateServiceCmd.Flags().StringP("description", "d", "Pagamento onere esempio", "Descrizione del servizio")
	generateServiceCmd.Flags().StringP("note", "n", "", "Note del servizio")
	generateServiceCmd.Flags().Int32P("acc_entry", "y", 1230, "Voce contabilità")
	generateServiceCmd.Flags().Int32P("acc_cap", "p", 245, "Capitolo contabilità")
	generateServiceCmd.Flags().Int32P("acc_art", "r", 1, "Articolo contabilità")
	generateServiceCmd.Flags().BoolP("split", "b", false, "Con bilancio")
}
