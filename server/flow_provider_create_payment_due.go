package server

import (
	"crypto/tls"
	"strconv"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models/siscom"
)

type FlowProviderCreatePaymentDue struct {
	Name          string
	Sctx          *ServerContext
	Payment       *models.Payment
	Service       *models.Service
	Tenant        *models.Tenant
	Request       *siscom.GeneraNuovoMovimentoIUVconTipologie
	Client        *soap.Client
	Siscom        siscom.Service
	Err           error
	Msg           string
	Status        bool
	Result        *siscom.GeneraNuovoMovimentoIUVconTipologieResponse
	IUV           string
	NoticeCode    string
	TransactionID string
}

func (flow *FlowProviderCreatePaymentDue) Exec() bool {
	flow.Name = "ProviderCreatePaymentDue"

	flow.Status = true &&
		flow.preparePayment() &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderCreatePaymentDue) preparePayment() bool {
	paymentDetailSplit := []*models.PaymentDetailSplit{}

	if flow.Service.Splitted && flow.Service.Split != nil {
		// fmt.Println("AAAA")
		// aaaa, _ := json.Marshal(flow.Service.Split)
		// fmt.Println(string(aaaa))

		for _, serviceSplit := range flow.Service.Split {
			serviceSplitAmount := serviceSplit.Amount

			paymentSplit := &models.PaymentDetailSplit{
				Code:   serviceSplit.Code,
				Amount: &serviceSplitAmount,
				Meta: &models.PaymentDetailSplitMeta{
					Key:         serviceSplit.Key,
					Description: serviceSplit.Description,
					AccEntry:    serviceSplit.AccEntry,
					AccCap:      serviceSplit.AccCap,
					AccArt:      serviceSplit.AccArt,
				},
			}
			paymentDetailSplit = append(paymentDetailSplit, paymentSplit)
		}
	}

	// fmt.Println("BBBB")
	// bbbb, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(bbbb))

	if flow.Payment.Payment.Split != nil {
		for _, incomingPaymentSplit := range flow.Payment.Payment.Split {
			if incomingPaymentSplit.Amount == nil {
				for index, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentDetailSplit = append(paymentDetailSplit[:index], paymentDetailSplit[index+1:]...)
					}
				}
			} else {
				for _, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentSplit.Amount = incomingPaymentSplit.Amount
					}
				}
			}
		}
	}

	// fmt.Println("CCCC")
	// cccc, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(cccc))
	// fmt.Println("DDDD")
	// dddd, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(dddd))

	flow.Payment.Payment.Split = paymentDetailSplit

	var totalAmount float64 = 0.0
	for _, paymentSplit := range flow.Payment.Payment.Split {
		totalAmount += *paymentSplit.Amount
	}

	// fmt.Println("EEEE")
	// eeee, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(eeee))

	if len(flow.Payment.Payment.Split) > 0 && flow.Payment.Payment.Amount != totalAmount {
		flow.Msg = "total amount and split amounts sum mismatching"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) prepareRequest() bool {
	referenceYear, err := strconv.Atoi(flow.Payment.CreatedAt.Format("2006"))
	if err != nil {
		flow.Err = err
		flow.Msg = "reference year conversion error while preparing due request"
		return false
	}

	var payerType, debitCodFiscale, debitPartitaIVA string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
		debitCodFiscale = flow.Payment.Payer.TaxIdentificationNumber
		debitPartitaIVA = ""
	} else {
		payerType = "G"
		debitCodFiscale = ""
		debitPartitaIVA = flow.Payment.Payer.TaxIdentificationNumber
	}

	listaMovimentoRemotoDett := &siscom.ArrayOfMovimentoRemotoDett{
		MovimentoRemotoDett: []*siscom.MovimentoRemotoDett{},
	}

	if flow.Payment.Payment.Split != nil {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			movimentoRemotoDett := &siscom.MovimentoRemotoDett{
				IDMovimentoRemotoDett: 0, // sempre zero?
				IDMovimentoRemoto:     0, // sempre zero?
				RifEnte:               flow.Tenant.Code,
				// IUV:                   "",
				ProceduraCodTipo: paymentSplit.Meta.Key,
				PagamentoCausale: paymentSplit.Meta.Description,
				PagamentoImporto: *paymentSplit.Amount,
				ContVoce:         paymentSplit.Meta.AccEntry,
				ContCapitolo:     paymentSplit.Meta.AccCap,
				ContArticolo:     paymentSplit.Meta.AccArt,
				ContAnnoAccert:   int32(referenceYear),
				ContNumAccert:    0, // sempre zero?
				ContNumSubAccert: 0, // sempre zero?
				// ContTipoContabilita:   "",
				// ContCodiceContabilita: "",
			}

			listaMovimentoRemotoDett.MovimentoRemotoDett = append(listaMovimentoRemotoDett.MovimentoRemotoDett, movimentoRemotoDett)
		}
	}

	// fmt.Println("FFFF")
	// ffff, _ := json.Marshal(listaDatiContabili)
	// fmt.Println(string(ffff))

	flow.Request = &siscom.GeneraNuovoMovimentoIUVconTipologie{
		PCodFiscaleEnte: flow.Tenant.TaxNumber,
		PMovimentoRemotoConDett: &siscom.MovimentoRemotoConDett{
			MovimentoRemoto: &siscom.MovimentoRemoto{
				IDMovimentoRemoto: 0,
				RifEnte:           flow.Tenant.Code,
				ProceduraNome:     flow.Tenant.AppName,
				ProceduraServizio: flow.Tenant.AppService,
				ProceduraCodTipo:  flow.Service.Key,
				ProceduraChiave:   flow.Tenant.AppKey,
				PagamentoCausale:  flow.Service.Description,
				PagamentoNote:     flow.Service.Note,
				PagamentoImporto:  0,
				TipoPagoPa:        0,
				CodiceServizio:    flow.Service.Code,
				CodiceEntrata:     flow.Service.EntryCode,
				// RifNumero:                  "",
				RifAnno:            strconv.Itoa(referenceYear),
				RifRata:            1,
				DebitNatura:        payerType,
				DebitRagSocCognome: flow.Payment.Payer.FamilyName,
				DebitNome:          flow.Payment.Payer.Name,
				DebitCodFiscale:    debitCodFiscale,
				DebitPartitaIVA:    debitPartitaIVA,
				// DebitCellulare:        "",
				DebitEMail:       flow.Payment.Payer.Email,
				DebitIndirizzo:   flow.Payment.Payer.StreetName,
				DebitCivico:      flow.Payment.Payer.BuildingNumber,
				DebitCap:         flow.Payment.Payer.PostalCode,
				DebitCitta:       flow.Payment.Payer.TownName,
				DebitProvincia:   flow.Payment.Payer.CountrySubdivision,
				DebitNazione:     flow.Payment.Payer.Country,
				ContVoce:         flow.Service.AccEntry,
				ContCapitolo:     flow.Service.AccCap,
				ContArticolo:     flow.Service.AccArt,
				ContAnnoAccert:   int32(referenceYear),
				ContNumAccert:    0, // sempre zero?
				ContNumSubAccert: 0, // sempre zero?
				// ContTipoContabilita:   "",
				// ContCodiceContabilita: "",
				DataEmissione:  soap.CreateXsdDateTime(flow.Payment.CreatedAt.Time, false),
				DataScadenza:   soap.CreateXsdDateTime(flow.Payment.Payment.ExpireAt.Time, false),
				DataScadStampa: soap.CreateXsdDateTime(flow.Payment.Payment.ExpireAt.Time, false),
				// IUV:                        "",
				// NumAvviso:                  "",
				SiscStato: 0, // sempre zero?
			},
			MovimentoRemotoDett: listaMovimentoRemotoDett,
		},
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("create_payment_due"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Siscom = siscom.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Siscom.GeneraNuovoMovimentoIUVconTipologie(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	requestResult := flow.Result.GeneraNuovoMovimentoIUVconTipologieResult.MovimentoRemoto.SiscStato == 10
	if !requestResult {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	flow.IUV = flow.Result.GeneraNuovoMovimentoIUVconTipologieResult.MovimentoRemoto.IUV
	flow.NoticeCode = flow.Result.GeneraNuovoMovimentoIUVconTipologieResult.MovimentoRemoto.NumAvviso
	flow.TransactionID = strconv.FormatInt(flow.Result.GeneraNuovoMovimentoIUVconTipologieResult.MovimentoRemoto.IDMovimentoRemoto, 10)

	timer.ObserveDuration()

	return true
}
