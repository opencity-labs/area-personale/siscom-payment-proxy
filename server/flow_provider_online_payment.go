package server

import (
	"crypto/tls"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models/siscom"
)

type FlowProviderOnlinePayment struct {
	Name    string
	Sctx    *ServerContext
	Payment *models.Payment
	Service *models.Service
	Tenant  *models.Tenant
	Request *siscom.LeggiLinkWISP_v3
	Client  *soap.Client
	Siscom  siscom.Service
	Err     error
	Msg     string
	Status  bool
	Result  *siscom.LeggiLinkWISP_v3Response
	Url     string
}

func (flow *FlowProviderOnlinePayment) Exec() bool {
	flow.Name = "ProviderOnlinePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderOnlinePayment) prepareRequest() bool {
	serverConfig := flow.Sctx.ServerConfig()

	iuvCode := flow.Payment.Payment.IUV

	flow.Request = &siscom.LeggiLinkWISP_v3{
		PCodFiscaleEnte: flow.Tenant.TaxNumber,
		PIUV:            iuvCode,
		PUrlRitorno:     serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID,
	}

	return true
}

func (flow *FlowProviderOnlinePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_online"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Siscom = siscom.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Siscom.LeggiLinkWISP_v3(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	requestResult := flow.Result.LeggiLinkWISP_v3Result
	if requestResult == "" || requestResult == "0|" {
		flow.Msg = "provider did not give us a URL for payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Url = requestResult

	return true
}
