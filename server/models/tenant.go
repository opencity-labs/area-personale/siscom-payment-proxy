package models

type Tenant struct {
	ID         string `json:"id"`
	Active     bool   `json:"active"`
	Endpoint   string `json:"endpoint"`
	Production bool   `json:"production"`
	Username   string `json:"username"`    // basic auth username
	Password   string `json:"password"`    // basic auth password
	TaxNumber  string `json:"tax_number"`  // pCodFiscaleEnte
	Code       string `json:"code"`        // RifEnte
	AppName    string `json:"app_name"`    // ProceduraNome
	AppService string `json:"app_service"` // ProceduraServizio
	AppKey     string `json:"app_key"`     // ProceduraChiave
}
