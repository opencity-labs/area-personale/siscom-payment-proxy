package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
)

var generateTenantCmd = &cobra.Command{
	Use:   "tenant",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated tenant json:")
		fmt.Println()

		endpoint, err := cmd.Flags().GetString("endpoint")
		if err != nil {
			panic(err)
		}
		production, err := cmd.Flags().GetBool("production")
		if err != nil {
			panic(err)
		}
		username, err := cmd.Flags().GetString("username")
		if err != nil {
			panic(err)
		}
		password, err := cmd.Flags().GetString("password")
		if err != nil {
			panic(err)
		}
		tax_number, err := cmd.Flags().GetString("tax_number")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		app_name, err := cmd.Flags().GetString("app_name")
		if err != nil {
			panic(err)
		}
		app_service, err := cmd.Flags().GetString("app_service")
		if err != nil {
			panic(err)
		}
		app_key, err := cmd.Flags().GetString("app_key")
		if err != nil {
			panic(err)
		}

		tenant := models.Tenant{
			ID:         uuid.NewV4().String(),
			Active:     true,
			Endpoint:   endpoint,
			Production: production,
			Username:   username,
			Password:   password,
			TaxNumber:  tax_number,
			Code:       code,
			AppName:    app_name,
			AppService: app_service,
			AppKey:     app_key,
		}

		bytes, err := json.MarshalIndent(tenant, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateTenantCmd)

	generateTenantCmd.Flags().StringP("endpoint", "e", "https://servizi2.siscom.eu/PagoPAwsGateway/ServiceGatewayPPA.asmx", "Endpoint del tenant")
	generateTenantCmd.Flags().Bool("production", false, "Modalità production?")
	generateTenantCmd.Flags().StringP("username", "u", "OpenCityLabs", "Username del tenant")
	generateTenantCmd.Flags().StringP("password", "p", "U0-45nGW#3f@7K!12", "Password del tenant")
	generateTenantCmd.Flags().StringP("tax_number", "t", "12345678901", "Codice fiscale del tenant")
	generateTenantCmd.Flags().StringP("code", "c", "12345678901", "Codice riferimento del tenant")
	generateTenantCmd.Flags().StringP("app_name", "n", "DittaPortale", "Nome procedura del tenant")
	generateTenantCmd.Flags().StringP("app_service", "s", "MioPortaleWeb", "Servizio procedura del tenant")
	generateTenantCmd.Flags().StringP("app_key", "k", "IDPortale12346", "Chiave procedura del tenant")
}
