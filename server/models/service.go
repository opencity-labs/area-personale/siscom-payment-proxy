package models

type Service struct {
	ID          string         `json:"id"`
	TenantID    string         `json:"tenant_id"`
	Active      bool           `json:"active"`
	Code        string         `json:"code"`                                       // CodiceServizio
	EntryCode   string         `json:"entry_code"`                                 // CodiceEntrata
	Key         string         `json:"key"`                                        // ProceduraCodTipo
	Description string         `json:"description"`                                // PagamentoCausale
	Note        string         `json:"note"`                                       // PagamentoNote,
	AccEntry    int32          `json:"acc_entry" description:"..." format:"int32"` // ContVoce
	AccCap      int32          `json:"acc_cap" description:"..." format:"int32"`   // ContCapitolo
	AccArt      int32          `json:"acc_art" description:"..." format:"int32"`   // ContArticolo
	Splitted    bool           `json:"splitted"`
	Split       []ServiceSplit `json:"split"`
}

type ServiceSplit struct {
	Code        string  `json:"split_code"`
	Amount      float64 `json:"split_amount" description:"..." format:"float"`
	Key         string  `json:"split_key"`                                        // ProceduraCodTipo
	Description string  `json:"split_description"`                                // PagamentoCausale
	AccEntry    int32   `json:"split_acc_entry" description:"..." format:"int32"` // ContVoce
	AccCap      int32   `json:"split_acc_cap" description:"..." format:"int32"`   // ContCapitolo
	AccArt      int32   `json:"split_acc_art" description:"..." format:"int32"`   // ContArticolo
}
