package siscom

import (
	"context"
	"encoding/xml"
	"time"

	"github.com/hooklift/gowsdl/soap"
)

// against "unused imports"
var _ time.Time
var _ xml.Name

type AnyType struct {
	InnerXML string `xml:",innerxml"`
}

type AnyURI string

type NCName string

type GeneraNuovoMovimentoIUV struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraNuovoMovimentoIUV"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PMovimentoRemoto *MovimentoRemoto `xml:"pMovimentoRemoto,omitempty" json:"pMovimentoRemoto,omitempty"`
}

type GeneraNuovoMovimentoIUVResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraNuovoMovimentoIUVResponse"`

	GeneraNuovoMovimentoIUVResult *MovimentoRemoto `xml:"GeneraNuovoMovimentoIUVResult,omitempty" json:"GeneraNuovoMovimentoIUVResult,omitempty"`
}

type GeneraNuovoMovimentoIUVconTipologie struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraNuovoMovimentoIUVconTipologie"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PMovimentoRemotoConDett *MovimentoRemotoConDett `xml:"pMovimentoRemotoConDett,omitempty" json:"pMovimentoRemotoConDett,omitempty"`
}

type GeneraNuovoMovimentoIUVconTipologieResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraNuovoMovimentoIUVconTipologieResponse"`

	GeneraNuovoMovimentoIUVconTipologieResult *MovimentoRemotoConDett `xml:"GeneraNuovoMovimentoIUVconTipologieResult,omitempty" json:"GeneraNuovoMovimentoIUVconTipologieResult,omitempty"`
}

type ModificaMovimentoIUV struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ModificaMovimentoIUV"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PMovimentoRemoto *MovimentoRemoto `xml:"pMovimentoRemoto,omitempty" json:"pMovimentoRemoto,omitempty"`
}

type ModificaMovimentoIUVResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ModificaMovimentoIUVResponse"`

	ModificaMovimentoIUVResult *MovimentoRemoto `xml:"ModificaMovimentoIUVResult,omitempty" json:"ModificaMovimentoIUVResult,omitempty"`
}

type ModificaMovimentoIUVconTipologie struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ModificaMovimentoIUVconTipologie"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PMovimentoRemotoConDett *MovimentoRemotoConDett `xml:"pMovimentoRemotoConDett,omitempty" json:"pMovimentoRemotoConDett,omitempty"`
}

type ModificaMovimentoIUVconTipologieResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ModificaMovimentoIUVconTipologieResponse"`

	ModificaMovimentoIUVconTipologieResult *MovimentoRemotoConDett `xml:"ModificaMovimentoIUVconTipologieResult,omitempty" json:"ModificaMovimentoIUVconTipologieResult,omitempty"`
}

type AnnullaMovimentoIUV struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA AnnullaMovimentoIUV"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIUVMovimento string `xml:"pIUVMovimento,omitempty" json:"pIUVMovimento,omitempty"`

	PCodiceServizio string `xml:"pCodiceServizio,omitempty" json:"pCodiceServizio,omitempty"`

	POpeAnnulla string `xml:"pOpeAnnulla,omitempty" json:"pOpeAnnulla,omitempty"`
}

type AnnullaMovimentoIUVResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA AnnullaMovimentoIUVResponse"`

	AnnullaMovimentoIUVResult *MovimentoRemoto `xml:"AnnullaMovimentoIUVResult,omitempty" json:"AnnullaMovimentoIUVResult,omitempty"`
}

type LeggiStatoMovimentoIUV struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiStatoMovimentoIUV"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIDMovimentoRemoto int64 `xml:"pIDMovimentoRemoto,omitempty" json:"pIDMovimentoRemoto,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`

	PProceduraNome string `xml:"pProceduraNome,omitempty" json:"pProceduraNome,omitempty"`

	PProceduraServizio string `xml:"pProceduraServizio,omitempty" json:"pProceduraServizio,omitempty"`

	PProceduraChiave string `xml:"pProceduraChiave,omitempty" json:"pProceduraChiave,omitempty"`
}

type LeggiStatoMovimentoIUVResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiStatoMovimentoIUVResponse"`

	LeggiStatoMovimentoIUVResult *MovimentoRemoto `xml:"LeggiStatoMovimentoIUVResult,omitempty" json:"LeggiStatoMovimentoIUVResult,omitempty"`
}

type LeggiStatoMovimentoIUVconTipologie struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiStatoMovimentoIUVconTipologie"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIDMovimentoRemoto int64 `xml:"pIDMovimentoRemoto,omitempty" json:"pIDMovimentoRemoto,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`

	PProceduraNome string `xml:"pProceduraNome,omitempty" json:"pProceduraNome,omitempty"`

	PProceduraServizio string `xml:"pProceduraServizio,omitempty" json:"pProceduraServizio,omitempty"`

	PProceduraChiave string `xml:"pProceduraChiave,omitempty" json:"pProceduraChiave,omitempty"`
}

type LeggiStatoMovimentoIUVconTipologieResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiStatoMovimentoIUVconTipologieResponse"`

	LeggiStatoMovimentoIUVconTipologieResult *MovimentoRemotoConDett `xml:"LeggiStatoMovimentoIUVconTipologieResult,omitempty" json:"LeggiStatoMovimentoIUVconTipologieResult,omitempty"`
}

type ElencoMovimentiIUV struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoMovimentiIUV"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PProceduraNome string `xml:"pProceduraNome,omitempty" json:"pProceduraNome,omitempty"`

	PProceduraServizio string `xml:"pProceduraServizio,omitempty" json:"pProceduraServizio,omitempty"`

	PDaData string `xml:"pDaData,omitempty" json:"pDaData,omitempty"`

	PAData string `xml:"pAData,omitempty" json:"pAData,omitempty"`

	PSoloPagati bool `xml:"pSoloPagati,omitempty" json:"pSoloPagati,omitempty"`
}

type ElencoMovimentiIUVResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoMovimentiIUVResponse"`

	ElencoMovimentiIUVResult *ArrayOfMovimentoRemoto `xml:"ElencoMovimentiIUVResult,omitempty" json:"ElencoMovimentiIUVResult,omitempty"`
}

type ElencoPagamentiIUV struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoPagamentiIUV"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PProceduraNome string `xml:"pProceduraNome,omitempty" json:"pProceduraNome,omitempty"`

	PProceduraServizio string `xml:"pProceduraServizio,omitempty" json:"pProceduraServizio,omitempty"`

	PDaData string `xml:"pDaData,omitempty" json:"pDaData,omitempty"`

	PAData string `xml:"pAData,omitempty" json:"pAData,omitempty"`
}

type ElencoPagamentiIUVResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoPagamentiIUVResponse"`

	ElencoPagamentiIUVResult *ArrayOfMovimentoRemoto `xml:"ElencoPagamentiIUVResult,omitempty" json:"ElencoPagamentiIUVResult,omitempty"`
}

type ElencoMovimentiIUV_PerDebitore struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoMovimentiIUV_PerDebitore"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PProceduraNome string `xml:"pProceduraNome,omitempty" json:"pProceduraNome,omitempty"`

	PProceduraServizio string `xml:"pProceduraServizio,omitempty" json:"pProceduraServizio,omitempty"`

	PDaData string `xml:"pDaData,omitempty" json:"pDaData,omitempty"`

	PAData string `xml:"pAData,omitempty" json:"pAData,omitempty"`

	PSoloPagati bool `xml:"pSoloPagati,omitempty" json:"pSoloPagati,omitempty"`

	PDebitCodFiscale string `xml:"pDebitCodFiscale,omitempty" json:"pDebitCodFiscale,omitempty"`

	PDebitPartitaIVA string `xml:"pDebitPartitaIVA,omitempty" json:"pDebitPartitaIVA,omitempty"`

	PSoloDaPagare bool `xml:"pSoloDaPagare,omitempty" json:"pSoloDaPagare,omitempty"`

	PCodiceServizio string `xml:"pCodiceServizio,omitempty" json:"pCodiceServizio,omitempty"`
}

type ElencoMovimentiIUV_PerDebitoreResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoMovimentiIUV_PerDebitoreResponse"`

	ElencoMovimentiIUV_PerDebitoreResult *ArrayOfMovimentoRemoto `xml:"ElencoMovimentiIUV_PerDebitoreResult,omitempty" json:"ElencoMovimentiIUV_PerDebitoreResult,omitempty"`
}

type GeneraPDF struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraPDF"`

	PCEIUVSuggerito string `xml:"pCEIUVSuggerito,omitempty" json:"pCEIUVSuggerito,omitempty"`

	PSchedaEnte_RagSociale string `xml:"pSchedaEnte_RagSociale,omitempty" json:"pSchedaEnte_RagSociale,omitempty"`

	PSchedaEnte_CodFiscale string `xml:"pSchedaEnte_CodFiscale,omitempty" json:"pSchedaEnte_CodFiscale,omitempty"`

	PSchedaEnte_SitoWeb string `xml:"pSchedaEnte_SitoWeb,omitempty" json:"pSchedaEnte_SitoWeb,omitempty"`

	PSchedaEnte_CBILL string `xml:"pSchedaEnte_CBILL,omitempty" json:"pSchedaEnte_CBILL,omitempty"`

	PSchedaEnte_PosteCC string `xml:"pSchedaEnte_PosteCC,omitempty" json:"pSchedaEnte_PosteCC,omitempty"`

	PSchedaEnte_PosteCCIntestaz string `xml:"pSchedaEnte_PosteCCIntestaz,omitempty" json:"pSchedaEnte_PosteCCIntestaz,omitempty"`

	PSchedaEnte_CodGNL string `xml:"pSchedaEnte_CodGNL,omitempty" json:"pSchedaEnte_CodGNL,omitempty"`

	PServizi_Codice string `xml:"pServizi_Codice,omitempty" json:"pServizi_Codice,omitempty"`

	PImportoPagamento float64 `xml:"pImportoPagamento,omitempty" json:"pImportoPagamento,omitempty"`

	PDebitCodFiscale string `xml:"pDebitCodFiscale,omitempty" json:"pDebitCodFiscale,omitempty"`

	PDebitPartitaIva string `xml:"pDebitPartitaIva,omitempty" json:"pDebitPartitaIva,omitempty"`

	PDebitRagSocCognome string `xml:"pDebitRagSocCognome,omitempty" json:"pDebitRagSocCognome,omitempty"`

	PDebitNome string `xml:"pDebitNome,omitempty" json:"pDebitNome,omitempty"`

	PCEIndirizzoDebitore string `xml:"pCEIndirizzoDebitore,omitempty" json:"pCEIndirizzoDebitore,omitempty"`

	PCECapDebitore string `xml:"pCECapDebitore,omitempty" json:"pCECapDebitore,omitempty"`

	PCELocalitaDebitore string `xml:"pCELocalitaDebitore,omitempty" json:"pCELocalitaDebitore,omitempty"`

	PCEProvinciaDebitore string `xml:"pCEProvinciaDebitore,omitempty" json:"pCEProvinciaDebitore,omitempty"`

	PCausalePagamento string `xml:"pCausalePagamento,omitempty" json:"pCausalePagamento,omitempty"`

	PCEDataScadenza string `xml:"pCEDataScadenza,omitempty" json:"pCEDataScadenza,omitempty"`

	PDataScadModello string `xml:"pDataScadModello,omitempty" json:"pDataScadModello,omitempty"`

	PCeNumAvviso string `xml:"pCeNumAvviso,omitempty" json:"pCeNumAvviso,omitempty"`

	PNote string `xml:"pNote,omitempty" json:"pNote,omitempty"`
}

type GeneraPDFResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraPDFResponse"`

	GeneraPDFResult []byte `xml:"GeneraPDFResult,omitempty" json:"GeneraPDFResult,omitempty"`
}

type GeneraPDF_DaID struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraPDF_DaID"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIDMovimentoRemoto int64 `xml:"pIDMovimentoRemoto,omitempty" json:"pIDMovimentoRemoto,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`
}

type GeneraPDF_DaIDResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA GeneraPDF_DaIDResponse"`

	GeneraPDF_DaIDResult []byte `xml:"GeneraPDF_DaIDResult,omitempty" json:"GeneraPDF_DaIDResult,omitempty"`
}

type ElencoServizi struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoServizi"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PPerPagamentiSpontanei bool `xml:"pPerPagamentiSpontanei,omitempty" json:"pPerPagamentiSpontanei,omitempty"`
}

type ElencoServiziResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA ElencoServiziResponse"`

	ElencoServiziResult *ArrayOfString `xml:"ElencoServiziResult,omitempty" json:"ElencoServiziResult,omitempty"`
}

type LeggiLinkWISP struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiLinkWISP"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`
}

type LeggiLinkWISPResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiLinkWISPResponse"`

	LeggiLinkWISPResult string `xml:"LeggiLinkWISPResult,omitempty" json:"LeggiLinkWISPResult,omitempty"`
}

type LeggiLinkWISP_v2 struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiLinkWISP_v2"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`

	PUrlRitorno string `xml:"pUrlRitorno,omitempty" json:"pUrlRitorno,omitempty"`
}

type LeggiLinkWISP_v2Response struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiLinkWISP_v2Response"`

	LeggiLinkWISP_v2Result string `xml:"LeggiLinkWISP_v2Result,omitempty" json:"LeggiLinkWISP_v2Result,omitempty"`
}

type LeggiLinkWISP_v3 struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiLinkWISP_v3"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`

	PUrlRitorno string `xml:"pUrlRitorno,omitempty" json:"pUrlRitorno,omitempty"`
}

type LeggiLinkWISP_v3Response struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA LeggiLinkWISP_v3Response"`

	LeggiLinkWISP_v3Result string `xml:"LeggiLinkWISP_v3Result,omitempty" json:"LeggiLinkWISP_v3Result,omitempty"`
}

type DownloadRTxml struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA DownloadRTxml"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PIUV string `xml:"pIUV,omitempty" json:"pIUV,omitempty"`
}

type DownloadRTxmlResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA DownloadRTxmlResponse"`

	DownloadRTxmlResult string `xml:"DownloadRTxmlResult,omitempty" json:"DownloadRTxmlResult,omitempty"`
}

type TestFunzionamento struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA TestFunzionamento"`

	PCodFiscaleEnte string `xml:"pCodFiscaleEnte,omitempty" json:"pCodFiscaleEnte,omitempty"`

	PCodServizio string `xml:"pCodServizio,omitempty" json:"pCodServizio,omitempty"`
}

type TestFunzionamentoResponse struct {
	XMLName xml.Name `xml:"http://pagopa.siscom.eu/ServiceGatewayPPA TestFunzionamentoResponse"`

	TestFunzionamentoResult string `xml:"TestFunzionamentoResult,omitempty" json:"TestFunzionamentoResult,omitempty"`
}

type MovimentoRemoto struct {
	IDMovimentoRemoto int64 `xml:"IDMovimentoRemoto,omitempty" json:"IDMovimentoRemoto,omitempty"`

	RifEnte string `xml:"RifEnte,omitempty" json:"RifEnte,omitempty"`

	ProceduraNome string `xml:"ProceduraNome,omitempty" json:"ProceduraNome,omitempty"`

	ProceduraServizio string `xml:"ProceduraServizio,omitempty" json:"ProceduraServizio,omitempty"`

	ProceduraCodTipo string `xml:"ProceduraCodTipo,omitempty" json:"ProceduraCodTipo,omitempty"`

	ProceduraChiave string `xml:"ProceduraChiave,omitempty" json:"ProceduraChiave,omitempty"`

	PagamentoCausale string `xml:"PagamentoCausale,omitempty" json:"PagamentoCausale,omitempty"`

	PagamentoNote string `xml:"PagamentoNote,omitempty" json:"PagamentoNote,omitempty"`

	PagamentoImporto float64 `xml:"PagamentoImporto,omitempty" json:"PagamentoImporto,omitempty"`

	TipoPagoPa int32 `xml:"TipoPagoPa,omitempty" json:"TipoPagoPa,omitempty"`

	CodiceServizio string `xml:"CodiceServizio,omitempty" json:"CodiceServizio,omitempty"`

	CodiceEntrata string `xml:"CodiceEntrata,omitempty" json:"CodiceEntrata,omitempty"`

	RifNumero string `xml:"RifNumero,omitempty" json:"RifNumero,omitempty"`

	RifAnno string `xml:"RifAnno,omitempty" json:"RifAnno,omitempty"`

	RifRata int32 `xml:"RifRata,omitempty" json:"RifRata,omitempty"`

	DebitNatura string `xml:"DebitNatura,omitempty" json:"DebitNatura,omitempty"`

	DebitRagSocCognome string `xml:"DebitRagSocCognome,omitempty" json:"DebitRagSocCognome,omitempty"`

	DebitNome string `xml:"DebitNome,omitempty" json:"DebitNome,omitempty"`

	DebitCodFiscale string `xml:"DebitCodFiscale,omitempty" json:"DebitCodFiscale,omitempty"`

	DebitPartitaIVA string `xml:"DebitPartitaIVA,omitempty" json:"DebitPartitaIVA,omitempty"`

	DebitCellulare string `xml:"DebitCellulare,omitempty" json:"DebitCellulare,omitempty"`

	DebitEMail string `xml:"DebitEMail,omitempty" json:"DebitEMail,omitempty"`

	DebitIndirizzo string `xml:"DebitIndirizzo,omitempty" json:"DebitIndirizzo,omitempty"`

	DebitCivico string `xml:"DebitCivico,omitempty" json:"DebitCivico,omitempty"`

	DebitCap string `xml:"DebitCap,omitempty" json:"DebitCap,omitempty"`

	DebitCitta string `xml:"DebitCitta,omitempty" json:"DebitCitta,omitempty"`

	DebitProvincia string `xml:"DebitProvincia,omitempty" json:"DebitProvincia,omitempty"`

	DebitNazione string `xml:"DebitNazione,omitempty" json:"DebitNazione,omitempty"`

	ContVoce int32 `xml:"ContVoce,omitempty" json:"ContVoce,omitempty"`

	ContCapitolo int32 `xml:"ContCapitolo,omitempty" json:"ContCapitolo,omitempty"`

	ContArticolo int32 `xml:"ContArticolo,omitempty" json:"ContArticolo,omitempty"`

	ContAnnoAccert int32 `xml:"ContAnnoAccert,omitempty" json:"ContAnnoAccert,omitempty"`

	ContNumAccert int32 `xml:"ContNumAccert,omitempty" json:"ContNumAccert,omitempty"`

	ContNumSubAccert int32 `xml:"ContNumSubAccert,omitempty" json:"ContNumSubAccert,omitempty"`

	ContTipoContabilita string `xml:"ContTipoContabilita,omitempty" json:"ContTipoContabilita,omitempty"`

	ContCodiceContabilita string `xml:"ContCodiceContabilita,omitempty" json:"ContCodiceContabilita,omitempty"`

	DataEmissione soap.XSDDateTime `xml:"DataEmissione,omitempty" json:"DataEmissione,omitempty"`

	DataScadenza soap.XSDDateTime `xml:"DataScadenza,omitempty" json:"DataScadenza,omitempty"`

	DataScadStampa soap.XSDDateTime `xml:"DataScadStampa,omitempty" json:"DataScadStampa,omitempty"`

	DataOraRichIUV soap.XSDDateTime `xml:"DataOraRichIUV,omitempty" json:"DataOraRichIUV,omitempty"`

	DataOraRicezIUV soap.XSDDateTime `xml:"DataOraRicezIUV,omitempty" json:"DataOraRicezIUV,omitempty"`

	AnnullatoDataOra soap.XSDDateTime `xml:"AnnullatoDataOra,omitempty" json:"AnnullatoDataOra,omitempty"`

	AnnullatoOpe string `xml:"AnnullatoOpe,omitempty" json:"AnnullatoOpe,omitempty"`

	DataOraIncasso soap.XSDDateTime `xml:"DataOraIncasso,omitempty" json:"DataOraIncasso,omitempty"`

	IUV string `xml:"IUV,omitempty" json:"IUV,omitempty"`

	NumAvviso string `xml:"NumAvviso,omitempty" json:"NumAvviso,omitempty"`

	HMAC string `xml:"HMAC,omitempty" json:"HMAC,omitempty"`

	SiscStato int32 `xml:"SiscStato,omitempty" json:"SiscStato,omitempty"`

	RisCodice string `xml:"RisCodice,omitempty" json:"RisCodice,omitempty"`

	RisDescrizione string `xml:"RisDescrizione,omitempty" json:"RisDescrizione,omitempty"`

	LogDataOra soap.XSDDateTime `xml:"LogDataOra,omitempty" json:"LogDataOra,omitempty"`

	LogOperatore string `xml:"LogOperatore,omitempty" json:"LogOperatore,omitempty"`

	LogOrigine string `xml:"LogOrigine,omitempty" json:"LogOrigine,omitempty"`

	BarCode_QRCode string `xml:"BarCode_QRCode,omitempty" json:"BarCode_QRCode,omitempty"`

	BarCode_QRCode_Cifrato string `xml:"BarCode_QRCode_Cifrato,omitempty" json:"BarCode_QRCode_Cifrato,omitempty"`

	BarCode_DataMatrix string `xml:"BarCode_DataMatrix,omitempty" json:"BarCode_DataMatrix,omitempty"`

	BarCode_DataMatrix_Cifrato string `xml:"BarCode_DataMatrix_Cifrato,omitempty" json:"BarCode_DataMatrix_Cifrato,omitempty"`

	BarCode_DataBar string `xml:"BarCode_DataBar,omitempty" json:"BarCode_DataBar,omitempty"`

	BarCode_DataBar_Cifrato string `xml:"BarCode_DataBar_Cifrato,omitempty" json:"BarCode_DataBar_Cifrato,omitempty"`
}

type MovimentoRemotoConDett struct {
	MovimentoRemoto *MovimentoRemoto `xml:"MovimentoRemoto,omitempty" json:"MovimentoRemoto,omitempty"`

	MovimentoRemotoDett *ArrayOfMovimentoRemotoDett `xml:"MovimentoRemotoDett,omitempty" json:"MovimentoRemotoDett,omitempty"`
}

type ArrayOfMovimentoRemotoDett struct {
	MovimentoRemotoDett []*MovimentoRemotoDett `xml:"MovimentoRemotoDett,omitempty" json:"MovimentoRemotoDett,omitempty"`
}

type MovimentoRemotoDett struct {
	IDMovimentoRemotoDett int64 `xml:"IDMovimentoRemotoDett,omitempty" json:"IDMovimentoRemotoDett,omitempty"`

	IDMovimentoRemoto int64 `xml:"IDMovimentoRemoto,omitempty" json:"IDMovimentoRemoto,omitempty"`

	RifEnte string `xml:"RifEnte,omitempty" json:"RifEnte,omitempty"`

	IUV string `xml:"IUV,omitempty" json:"IUV,omitempty"`

	ProceduraCodTipo string `xml:"ProceduraCodTipo,omitempty" json:"ProceduraCodTipo,omitempty"`

	PagamentoCausale string `xml:"PagamentoCausale,omitempty" json:"PagamentoCausale,omitempty"`

	PagamentoImporto float64 `xml:"PagamentoImporto,omitempty" json:"PagamentoImporto,omitempty"`

	ContVoce int32 `xml:"ContVoce,omitempty" json:"ContVoce,omitempty"`

	ContCapitolo int32 `xml:"ContCapitolo,omitempty" json:"ContCapitolo,omitempty"`

	ContArticolo int32 `xml:"ContArticolo,omitempty" json:"ContArticolo,omitempty"`

	ContAnnoAccert int32 `xml:"ContAnnoAccert,omitempty" json:"ContAnnoAccert,omitempty"`

	ContNumAccert int32 `xml:"ContNumAccert,omitempty" json:"ContNumAccert,omitempty"`

	ContNumSubAccert int32 `xml:"ContNumSubAccert,omitempty" json:"ContNumSubAccert,omitempty"`

	ContTipoContabilita string `xml:"ContTipoContabilita,omitempty" json:"ContTipoContabilita,omitempty"`

	ContCodiceContabilita string `xml:"ContCodiceContabilita,omitempty" json:"ContCodiceContabilita,omitempty"`
}

type ArrayOfMovimentoRemoto struct {
	MovimentoRemoto []*MovimentoRemoto `xml:"MovimentoRemoto,omitempty" json:"MovimentoRemoto,omitempty"`
}

type ArrayOfString struct {
	Astring []*string `xml:"string,omitempty" json:"string,omitempty"`
}

type Service interface {
	GeneraNuovoMovimentoIUV(request *GeneraNuovoMovimentoIUV) (*GeneraNuovoMovimentoIUVResponse, error)

	GeneraNuovoMovimentoIUVContext(ctx context.Context, request *GeneraNuovoMovimentoIUV) (*GeneraNuovoMovimentoIUVResponse, error)

	GeneraNuovoMovimentoIUVconTipologie(request *GeneraNuovoMovimentoIUVconTipologie) (*GeneraNuovoMovimentoIUVconTipologieResponse, error)

	GeneraNuovoMovimentoIUVconTipologieContext(ctx context.Context, request *GeneraNuovoMovimentoIUVconTipologie) (*GeneraNuovoMovimentoIUVconTipologieResponse, error)

	ModificaMovimentoIUV(request *ModificaMovimentoIUV) (*ModificaMovimentoIUVResponse, error)

	ModificaMovimentoIUVContext(ctx context.Context, request *ModificaMovimentoIUV) (*ModificaMovimentoIUVResponse, error)

	ModificaMovimentoIUVconTipologie(request *ModificaMovimentoIUVconTipologie) (*ModificaMovimentoIUVconTipologieResponse, error)

	ModificaMovimentoIUVconTipologieContext(ctx context.Context, request *ModificaMovimentoIUVconTipologie) (*ModificaMovimentoIUVconTipologieResponse, error)

	AnnullaMovimentoIUV(request *AnnullaMovimentoIUV) (*AnnullaMovimentoIUVResponse, error)

	AnnullaMovimentoIUVContext(ctx context.Context, request *AnnullaMovimentoIUV) (*AnnullaMovimentoIUVResponse, error)

	LeggiStatoMovimentoIUV(request *LeggiStatoMovimentoIUV) (*LeggiStatoMovimentoIUVResponse, error)

	LeggiStatoMovimentoIUVContext(ctx context.Context, request *LeggiStatoMovimentoIUV) (*LeggiStatoMovimentoIUVResponse, error)

	LeggiStatoMovimentoIUVconTipologie(request *LeggiStatoMovimentoIUVconTipologie) (*LeggiStatoMovimentoIUVconTipologieResponse, error)

	LeggiStatoMovimentoIUVconTipologieContext(ctx context.Context, request *LeggiStatoMovimentoIUVconTipologie) (*LeggiStatoMovimentoIUVconTipologieResponse, error)

	ElencoMovimentiIUV(request *ElencoMovimentiIUV) (*ElencoMovimentiIUVResponse, error)

	ElencoMovimentiIUVContext(ctx context.Context, request *ElencoMovimentiIUV) (*ElencoMovimentiIUVResponse, error)

	ElencoPagamentiIUV(request *ElencoPagamentiIUV) (*ElencoPagamentiIUVResponse, error)

	ElencoPagamentiIUVContext(ctx context.Context, request *ElencoPagamentiIUV) (*ElencoPagamentiIUVResponse, error)

	ElencoMovimentiIUV_PerDebitore(request *ElencoMovimentiIUV_PerDebitore) (*ElencoMovimentiIUV_PerDebitoreResponse, error)

	ElencoMovimentiIUV_PerDebitoreContext(ctx context.Context, request *ElencoMovimentiIUV_PerDebitore) (*ElencoMovimentiIUV_PerDebitoreResponse, error)

	GeneraPDF(request *GeneraPDF) (*GeneraPDFResponse, error)

	GeneraPDFContext(ctx context.Context, request *GeneraPDF) (*GeneraPDFResponse, error)

	GeneraPDF_DaID(request *GeneraPDF_DaID) (*GeneraPDF_DaIDResponse, error)

	GeneraPDF_DaIDContext(ctx context.Context, request *GeneraPDF_DaID) (*GeneraPDF_DaIDResponse, error)

	ElencoServizi(request *ElencoServizi) (*ElencoServiziResponse, error)

	ElencoServiziContext(ctx context.Context, request *ElencoServizi) (*ElencoServiziResponse, error)

	LeggiLinkWISP(request *LeggiLinkWISP) (*LeggiLinkWISPResponse, error)

	LeggiLinkWISPContext(ctx context.Context, request *LeggiLinkWISP) (*LeggiLinkWISPResponse, error)

	LeggiLinkWISP_v2(request *LeggiLinkWISP_v2) (*LeggiLinkWISP_v2Response, error)

	LeggiLinkWISP_v2Context(ctx context.Context, request *LeggiLinkWISP_v2) (*LeggiLinkWISP_v2Response, error)

	LeggiLinkWISP_v3(request *LeggiLinkWISP_v3) (*LeggiLinkWISP_v3Response, error)

	LeggiLinkWISP_v3Context(ctx context.Context, request *LeggiLinkWISP_v3) (*LeggiLinkWISP_v3Response, error)

	DownloadRTxml(request *DownloadRTxml) (*DownloadRTxmlResponse, error)

	DownloadRTxmlContext(ctx context.Context, request *DownloadRTxml) (*DownloadRTxmlResponse, error)

	TestFunzionamento(request *TestFunzionamento) (*TestFunzionamentoResponse, error)

	TestFunzionamentoContext(ctx context.Context, request *TestFunzionamento) (*TestFunzionamentoResponse, error)
}

type service struct {
	client *soap.Client
}

func NewService(client *soap.Client) Service {
	return &service{
		client: client,
	}
}

func (service *service) GeneraNuovoMovimentoIUVContext(ctx context.Context, request *GeneraNuovoMovimentoIUV) (*GeneraNuovoMovimentoIUVResponse, error) {
	response := new(GeneraNuovoMovimentoIUVResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/GeneraNuovoMovimentoIUV", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) GeneraNuovoMovimentoIUV(request *GeneraNuovoMovimentoIUV) (*GeneraNuovoMovimentoIUVResponse, error) {
	return service.GeneraNuovoMovimentoIUVContext(
		context.Background(),
		request,
	)
}

func (service *service) GeneraNuovoMovimentoIUVconTipologieContext(ctx context.Context, request *GeneraNuovoMovimentoIUVconTipologie) (*GeneraNuovoMovimentoIUVconTipologieResponse, error) {
	response := new(GeneraNuovoMovimentoIUVconTipologieResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/GeneraNuovoMovimentoIUVconTipologie", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) GeneraNuovoMovimentoIUVconTipologie(request *GeneraNuovoMovimentoIUVconTipologie) (*GeneraNuovoMovimentoIUVconTipologieResponse, error) {
	return service.GeneraNuovoMovimentoIUVconTipologieContext(
		context.Background(),
		request,
	)
}

func (service *service) ModificaMovimentoIUVContext(ctx context.Context, request *ModificaMovimentoIUV) (*ModificaMovimentoIUVResponse, error) {
	response := new(ModificaMovimentoIUVResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/ModificaMovimentoIUV", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ModificaMovimentoIUV(request *ModificaMovimentoIUV) (*ModificaMovimentoIUVResponse, error) {
	return service.ModificaMovimentoIUVContext(
		context.Background(),
		request,
	)
}

func (service *service) ModificaMovimentoIUVconTipologieContext(ctx context.Context, request *ModificaMovimentoIUVconTipologie) (*ModificaMovimentoIUVconTipologieResponse, error) {
	response := new(ModificaMovimentoIUVconTipologieResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/ModificaMovimentoIUVconTipologie", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ModificaMovimentoIUVconTipologie(request *ModificaMovimentoIUVconTipologie) (*ModificaMovimentoIUVconTipologieResponse, error) {
	return service.ModificaMovimentoIUVconTipologieContext(
		context.Background(),
		request,
	)
}

func (service *service) AnnullaMovimentoIUVContext(ctx context.Context, request *AnnullaMovimentoIUV) (*AnnullaMovimentoIUVResponse, error) {
	response := new(AnnullaMovimentoIUVResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/AnnullaMovimentoIUV", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) AnnullaMovimentoIUV(request *AnnullaMovimentoIUV) (*AnnullaMovimentoIUVResponse, error) {
	return service.AnnullaMovimentoIUVContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiStatoMovimentoIUVContext(ctx context.Context, request *LeggiStatoMovimentoIUV) (*LeggiStatoMovimentoIUVResponse, error) {
	response := new(LeggiStatoMovimentoIUVResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/LeggiStatoMovimentoIUV", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiStatoMovimentoIUV(request *LeggiStatoMovimentoIUV) (*LeggiStatoMovimentoIUVResponse, error) {
	return service.LeggiStatoMovimentoIUVContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiStatoMovimentoIUVconTipologieContext(ctx context.Context, request *LeggiStatoMovimentoIUVconTipologie) (*LeggiStatoMovimentoIUVconTipologieResponse, error) {
	response := new(LeggiStatoMovimentoIUVconTipologieResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/LeggiStatoMovimentoIUVconTipologie", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiStatoMovimentoIUVconTipologie(request *LeggiStatoMovimentoIUVconTipologie) (*LeggiStatoMovimentoIUVconTipologieResponse, error) {
	return service.LeggiStatoMovimentoIUVconTipologieContext(
		context.Background(),
		request,
	)
}

func (service *service) ElencoMovimentiIUVContext(ctx context.Context, request *ElencoMovimentiIUV) (*ElencoMovimentiIUVResponse, error) {
	response := new(ElencoMovimentiIUVResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/ElencoMovimentiIUV", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ElencoMovimentiIUV(request *ElencoMovimentiIUV) (*ElencoMovimentiIUVResponse, error) {
	return service.ElencoMovimentiIUVContext(
		context.Background(),
		request,
	)
}

func (service *service) ElencoPagamentiIUVContext(ctx context.Context, request *ElencoPagamentiIUV) (*ElencoPagamentiIUVResponse, error) {
	response := new(ElencoPagamentiIUVResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/ElencoPagamentiIUV", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ElencoPagamentiIUV(request *ElencoPagamentiIUV) (*ElencoPagamentiIUVResponse, error) {
	return service.ElencoPagamentiIUVContext(
		context.Background(),
		request,
	)
}

func (service *service) ElencoMovimentiIUV_PerDebitoreContext(ctx context.Context, request *ElencoMovimentiIUV_PerDebitore) (*ElencoMovimentiIUV_PerDebitoreResponse, error) {
	response := new(ElencoMovimentiIUV_PerDebitoreResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/ElencoMovimentiIUV_PerDebitore", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ElencoMovimentiIUV_PerDebitore(request *ElencoMovimentiIUV_PerDebitore) (*ElencoMovimentiIUV_PerDebitoreResponse, error) {
	return service.ElencoMovimentiIUV_PerDebitoreContext(
		context.Background(),
		request,
	)
}

func (service *service) GeneraPDFContext(ctx context.Context, request *GeneraPDF) (*GeneraPDFResponse, error) {
	response := new(GeneraPDFResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/GeneraPDF", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) GeneraPDF(request *GeneraPDF) (*GeneraPDFResponse, error) {
	return service.GeneraPDFContext(
		context.Background(),
		request,
	)
}

func (service *service) GeneraPDF_DaIDContext(ctx context.Context, request *GeneraPDF_DaID) (*GeneraPDF_DaIDResponse, error) {
	response := new(GeneraPDF_DaIDResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/GeneraPDF_DaID", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) GeneraPDF_DaID(request *GeneraPDF_DaID) (*GeneraPDF_DaIDResponse, error) {
	return service.GeneraPDF_DaIDContext(
		context.Background(),
		request,
	)
}

func (service *service) ElencoServiziContext(ctx context.Context, request *ElencoServizi) (*ElencoServiziResponse, error) {
	response := new(ElencoServiziResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/ElencoServizi", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ElencoServizi(request *ElencoServizi) (*ElencoServiziResponse, error) {
	return service.ElencoServiziContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiLinkWISPContext(ctx context.Context, request *LeggiLinkWISP) (*LeggiLinkWISPResponse, error) {
	response := new(LeggiLinkWISPResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/LeggiLinkWISP", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiLinkWISP(request *LeggiLinkWISP) (*LeggiLinkWISPResponse, error) {
	return service.LeggiLinkWISPContext(
		context.Background(),
		request,
	)
}

func (service *service) LeggiLinkWISP_v2Context(ctx context.Context, request *LeggiLinkWISP_v2) (*LeggiLinkWISP_v2Response, error) {
	response := new(LeggiLinkWISP_v2Response)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/LeggiLinkWISP_v2", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiLinkWISP_v2(request *LeggiLinkWISP_v2) (*LeggiLinkWISP_v2Response, error) {
	return service.LeggiLinkWISP_v2Context(
		context.Background(),
		request,
	)
}

func (service *service) LeggiLinkWISP_v3Context(ctx context.Context, request *LeggiLinkWISP_v3) (*LeggiLinkWISP_v3Response, error) {
	response := new(LeggiLinkWISP_v3Response)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/LeggiLinkWISP_v3", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) LeggiLinkWISP_v3(request *LeggiLinkWISP_v3) (*LeggiLinkWISP_v3Response, error) {
	return service.LeggiLinkWISP_v3Context(
		context.Background(),
		request,
	)
}

func (service *service) DownloadRTxmlContext(ctx context.Context, request *DownloadRTxml) (*DownloadRTxmlResponse, error) {
	response := new(DownloadRTxmlResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/DownloadRTxml", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) DownloadRTxml(request *DownloadRTxml) (*DownloadRTxmlResponse, error) {
	return service.DownloadRTxmlContext(
		context.Background(),
		request,
	)
}

func (service *service) TestFunzionamentoContext(ctx context.Context, request *TestFunzionamento) (*TestFunzionamentoResponse, error) {
	response := new(TestFunzionamentoResponse)
	err := service.client.CallContext(ctx, "http://pagopa.siscom.eu/ServiceGatewayPPA/TestFunzionamento", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) TestFunzionamento(request *TestFunzionamento) (*TestFunzionamentoResponse, error) {
	return service.TestFunzionamentoContext(
		context.Background(),
		request,
	)
}
