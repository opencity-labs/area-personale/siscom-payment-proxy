package server

import (
	"crypto/tls"
	"encoding/base64"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models/siscom"
)

type FlowProviderNoticePayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *siscom.GeneraPDF
	Client   *soap.Client
	Siscom   siscom.Service
	Err      error
	Msg      string
	Status   bool
	Result   *siscom.GeneraPDFResponse
	Received bool
	Notice   []byte
}

func (flow *FlowProviderNoticePayment) Exec() bool {
	flow.Name = "ProviderNoticePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderNoticePayment) prepareRequest() bool {
	var debitCodFiscale, debitPartitaIVA string
	if flow.Payment.Payer.Type == "human" {
		debitCodFiscale = flow.Payment.Payer.TaxIdentificationNumber
		debitPartitaIVA = ""
	} else {
		debitCodFiscale = ""
		debitPartitaIVA = flow.Payment.Payer.TaxIdentificationNumber
	}

	flow.Request = &siscom.GeneraPDF{
		PCEIUVSuggerito: flow.Payment.Payment.IUV,
		PCeNumAvviso:    flow.Payment.Payment.NoticeCode,
		// PSchedaEnte_RagSociale: "",
		PSchedaEnte_CodFiscale: flow.Tenant.TaxNumber,
		// PSchedaEnte_SitoWeb: "",
		// PSchedaEnte_CBILL: "",
		// PSchedaEnte_PosteCC: "",
		// PSchedaEnte_PosteCCIntestaz: "",
		// PSchedaEnte_CodGNL: "",
		PServizi_Codice:      flow.Service.Code,
		PImportoPagamento:    flow.Payment.Payment.Amount,
		PDebitCodFiscale:     debitCodFiscale,
		PDebitPartitaIva:     debitPartitaIVA,
		PDebitRagSocCognome:  flow.Payment.Payer.FamilyName,
		PDebitNome:           flow.Payment.Payer.Name,
		PCEIndirizzoDebitore: flow.Payment.Payer.StreetName,
		PCECapDebitore:       flow.Payment.Payer.PostalCode,
		PCELocalitaDebitore:  flow.Payment.Payer.TownName,
		PCEProvinciaDebitore: flow.Payment.Payer.CountrySubdivision,
		PCausalePagamento:    flow.Service.Description,
		PCEDataScadenza:      flow.Payment.Payment.ExpireAt.Time.Local().Format("2006-01-02"),
		PDataScadModello:     flow.Payment.Payment.ExpireAt.Time.Local().Format("2006-01-02"),
		PNote:                flow.Service.Note,
	}

	return true
}

func (flow *FlowProviderNoticePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_notice"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Siscom = siscom.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Siscom.GeneraPDF(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	requestResult := flow.Result.GeneraPDFResult
	if requestResult == nil {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	notice := string(requestResult)
	if notice == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Notice, err = base64.StdEncoding.DecodeString(notice)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	flow.Received = true

	return true
}
