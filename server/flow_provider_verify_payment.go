package server

import (
	"crypto/tls"
	"strconv"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models/siscom"
)

type FlowProviderVerifyPayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	Request   *siscom.LeggiStatoMovimentoIUVconTipologie
	Client    *soap.Client
	Siscom    siscom.Service
	Err       error
	Msg       string
	Status    bool
	Result    *siscom.LeggiStatoMovimentoIUVconTipologieResponse
	Completed bool
	Payed     bool
	PaidAt    models.Time
	Receipt   []byte
}

func (flow *FlowProviderVerifyPayment) Exec() bool {
	flow.Name = "ProviderVerifyPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderVerifyPayment) prepareRequest() bool {
	transactionId, err := strconv.Atoi(flow.Payment.Payment.TransactionID)
	if err != nil {
		transactionId = 0
	}

	flow.Request = &siscom.LeggiStatoMovimentoIUVconTipologie{
		PCodFiscaleEnte:    flow.Tenant.TaxNumber,
		PIDMovimentoRemoto: int64(transactionId),
		PIUV:               flow.Payment.Payment.IUV,
		PProceduraNome:     flow.Tenant.AppName,
		PProceduraServizio: flow.Tenant.AppService,
		PProceduraChiave:   flow.Tenant.AppKey,
	}

	return true
}

func (flow *FlowProviderVerifyPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_verify"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Siscom = siscom.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Siscom.LeggiStatoMovimentoIUVconTipologie(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting verify request"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	timer.ObserveDuration()

	requestResult := flow.Result.LeggiStatoMovimentoIUVconTipologieResult.MovimentoRemoto.SiscStato
	flow.Completed = requestResult == 20 || requestResult == 90
	flow.Payed = requestResult == 20
	flow.PaidAt = models.Time{Time: flow.Result.LeggiStatoMovimentoIUVconTipologieResult.MovimentoRemoto.DataOraIncasso.ToGoTime()}

	return true
}
