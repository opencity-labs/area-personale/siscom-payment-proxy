package server

import (
	"crypto/tls"
	"encoding/base64"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/siscom-payment-proxy/server/models/siscom"
)

type FlowProviderReceiptPayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *siscom.DownloadRTxml
	Client   *soap.Client
	Siscom   siscom.Service
	Err      error
	Msg      string
	Status   bool
	Result   *siscom.DownloadRTxmlResponse
	Received bool
	Receipt  []byte
}

func (flow *FlowProviderReceiptPayment) Exec() bool {
	flow.Name = "ProviderReceiptPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderReceiptPayment) prepareRequest() bool {
	flow.Request = &siscom.DownloadRTxml{
		PCodFiscaleEnte: flow.Tenant.TaxNumber,
		PIUV:            flow.Payment.Payment.IUV,
	}

	return true
}

func (flow *FlowProviderReceiptPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_receipt"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint, soap.WithBasicAuth(flow.Tenant.Username, flow.Tenant.Password), soap.WithTLS(&tls.Config{Renegotiation: tls.RenegotiateOnceAsClient}))

	flow.Siscom = siscom.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Siscom.DownloadRTxml(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	requestResult := flow.Result.DownloadRTxmlResult
	receipt := string(requestResult)
	if receipt == "" {
		flow.Msg = "provider response empty"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Receipt, err = base64.StdEncoding.DecodeString(receipt)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Received = true

	return true
}
